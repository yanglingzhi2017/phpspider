<?php
/* https://www.jianshu.com/p/01052508ea7c 

https://blog.csdn.net/oBaiLaTu12/article/details/81289691
*/

require './vendor/autoload.php';
use phpspider\core\phpspider;









/* Do NOT delete this comment */
/* 不要删除这段注释 */

$configs = array(
'name' => '百度',
'log_show' =>false,
'tasknum' => 1,
'user_agent' => array(
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
    "Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_3 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G34 Safari/601.1",
    "Mozilla/5.0 (Linux; U; Android 6.0.1;zh_cn; Le X820 Build/FEXCNFN5801507014S) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/49.0.0.0 Mobile Safari/537.36 EUI Browser/5.8.015S",
),
'client_ip' => array(
    '192.168.0.2', 
    '192.168.0.3',
    '192.168.0.4',
),

//数据库配置
'db_config' => array(
'host'  => '127.0.0.1',
'port'  => 3306,
'user'  => 'root',
'pass'  => 'root',
'name'  => 'demo',
),
'export' => array(
'type' => 'db',
'table' => 'phone',  // 如果数据表没有数据新增请检查表结构和字段名是否匹配
),
//爬取的域名列表  
'domains' => array(

'cd.meituan.com',
	'sh.meituan.com',
	'gz.meituan.com',
	'www.meituan.com',
    'bj.meituan.com',
	


), 
//抓取的起点
'scan_urls' => array(
    'https://gz.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
/* 	'https://bj.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://sh.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://cd.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://tj.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://nz.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://dg.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://hy.meituan.com/s/%E7%BE%8E%E5%AE%B9/',
	'https://cs.meituan.com/jiankangliren/1429433/',
	'https://hz.meituan.com/jiankangliren/1544554/', */
),
//列表页实例
'list_url_regexes' => array(
    "https://www.meituan.com/jiankangliren/\d+"
),
//内容页实例
//  \d+  指的是变量
  'content_url_regexes' => array(
    "https://www.meituan.com/jiankangliren/\d+"
),  
'max_try' => 5,

'fields' => array(
    array(
        'name' => "address",
		// 'selector_type' => 'regex',
    // 'selector' => "//div[@class='seller-info-body']", 
	//	 'selector' => '#<div\sclass="seller-info-body">([^\d]+)</div>#i', // regex抽取规则
		 
		 
    "selector" => "//div[contains(@class,'seller-info-body')]//div/a/span",
        'required' => true,
    ), 
     array(
        'name' => "phone",
     //   'selector' => "//div[@class='seller-info-body']",
	   "selector" => "//div[contains(@class,'seller-info-body')]//div[2]//span[2]",
	 
        'required' => true,
    ), 
	
	
	array(
	        'name' => "name",	 
		 
    "selector" => "//h1[contains(@class,'seller-name')]",
        'required' => true,	
	),
	
),
);

$spider = new phpspider($configs);



$spider->start();